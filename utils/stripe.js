const Stripe = require('stripe');
const stripe = Stripe('sk_test_4eC39HqLyjWDarjtT1zdp7dc');

exports.chargeCreditCard = async (creditCard, value) => {
  const token = await stripe.tokens.create({
    card: {
      number: creditCard.number,
      exp_month: creditCard.expM,
      exp_year: creditCard.expY,
      cvc: creditCard.cvc,
    }
  });
  const charge = await stripe.charges.create({
    amount: 2000,
    currency: 'usd',
    source: token.id,
    description: 'Sample Charge',
  });
  return charge;
};