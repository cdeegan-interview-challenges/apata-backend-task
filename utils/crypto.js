const { createCipheriv, createDecipheriv, randomBytes } = require('node:crypto');

// createHash('sha256').update(String('some_random_string')).digest('base64').substring(0, 32);
const ENCRYPTION_KEY = 'n4bQgYhMfWWaL+qgxVrQFaO/TxsrC4Is';

exports.encryptCardNumber = (plaintextCardNumber) => {
  const initialisationVector = randomBytes(16);
  const cipher = createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), initialisationVector);
  const encryptedCardNumber = cipher.update(plaintextCardNumber);
  const finalBuffer = Buffer.concat([encryptedCardNumber, cipher.final()]);
  return initialisationVector.toString('hex') + ':' + finalBuffer.toString('hex');
};

exports.decryptCardNumber = (encryptedCardNumber) => {
  const encryptedSegments = encryptedCardNumber.split(':');
  const initialisationVector = Buffer.from(encryptedSegments[0], 'hex');
  const encryptedText = Buffer.from(encryptedSegments[1], 'hex');
  const decipher = createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), initialisationVector);
  const decryptedBuffer = decipher.update(encryptedText);
  return Buffer.concat([decryptedBuffer, decipher.final()]).toString();
};
