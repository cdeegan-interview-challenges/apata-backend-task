'use strict';
const { Model, Sequelize } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CreditCard extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  CreditCard.init({
    number: {
      type: DataTypes.STRING
    },
    cardholderName: {
      type: DataTypes.STRING
    },
    balance: {
      type: DataTypes.NUMBER
    },
    limit: {
      type: DataTypes.NUMBER
    }
  }, {
    sequelize,
    modelName: 'CreditCard',
  });
  return CreditCard;
};