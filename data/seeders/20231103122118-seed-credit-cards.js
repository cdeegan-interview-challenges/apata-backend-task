'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('CreditCards', [{
      // 4111111111111111
      number: '966fe2edf66a6412688725825f69427a:63b6e31390294dddbff39835a4cc5f5dac531ffe5653683c348050338a767683',
      cardholderName: 'John Doe',
      balance: 250000,
      limit: 250000,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      // 4141414141414141
      number: 'f8314e5091dedc0425b8eff138e1771c:2911632b0b107c580fb7541f7e25d6caf9bf3aabf57bf87492b77f91d9c0cbd8',
      cardholderName: 'Jane Appleseed',
      balance: 0,
      limit: 550000,
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('CreditCards');
  }
};
