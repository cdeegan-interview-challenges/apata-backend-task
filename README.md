

# Apata Backend Task
## Usage
This server can be run using the command line, as follows.
```
npm run start
```
The dev server can be run using the command line, as follows. It will auto-reload when files are changed.
```
npm run dev
```
Tests can be run, as follows.
```
npm test
```
By default, the server will run at `localhost:3000`, and the credit card API is available at `/credit-cards`. Full API documentation is available here: [API Documentation](https://documenter.getpostman.com/view/446215/2s9YXe94ju)
## Installation
Clone the repository and run `npm install` inside the downloaded project folder. 
## Dependencies
- Tested with Node v18.12.1
## Specification
See SPEC.md.
## How it works
Credit card numbers are encrypted using AES-256. The initialisation vector (IV) for each credit card record is unique, which is much more secure than using the same IV for every card.

Credit card charges are first checked against the existing account balance and limit. Charges will fail if the amount would put the account over the limit. Charges are performed against the Stripe API using the [Stripe Node SDK](https://stripe.com/docs/libraries#server-side-libraries). Valid test cards are listed in the [Stripe documentation](https://stripe.com/docs/testing#cards).
## Observations
- using a different IV for each record makes it impossible to search by the unencrypted value, meaning the entire dataset needs to be retrieved and unencypted one-by-one. This is a **security over performance tradeoff**. A workaround might be to hash the value and search by the hash instead, although storing the hash has its own security downsides
- we should take advantage of any security features offered by the database, even when the data is encrypted
- we should not be passing the credit card as a URL parameter, URLs are often logged on systems in plaintext
- we shouldn't facilitate the display of all credit card records
- we should use tokenisation via a third-party to comply with PCI, and not store credit card information at all
- access to the sensitive data in this API should be restricted
- ⚠️🚨⚠️**this API has obvious compliance issues and is for observational use only**⚠️🚨⚠️
## Room for Improvement
- more accurate API error messages
- the encryption key for card numbers should be stored in a passphrase-protected PGP file instead of in plaintext in the code
- Additional testing around Stripe dependency, mocking databases, card limits and charging/crediting a card
- Tidier, modular code, e.g. integrate validation with Sequelize models
- General clean code + optimisation sweep