var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var creditCardsRouter = require('./routes/credit-cards');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/credit-cards', creditCardsRouter);
app.use((err, req, res, next) => {
  if (err.isBoom) {
    return res.status(err.output.payload.statusCode).send(err.output.payload.message);
  }
  res.status(500).send('An error occurred');
})

module.exports = app;
