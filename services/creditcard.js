const { encryptCardNumber, decryptCardNumber } = require('../utils/crypto');
const { CreditCard } = require('../data/models');
const { Sequelize } = require('sequelize');

exports.getAllCreditCards = async () => {
  const cards = await CreditCard.findAll();
  return cards.map((card) => {
    return { ...card.dataValues, number: decryptCardNumber(card.number) };
  });
};

exports.getCreditCardByUnencryptedNumber = async (targetCardNumber) => {
  const allCards = await CreditCard.findAll();
  let targetCard = null;
  allCards.forEach((card) => {
    const cardNumber = decryptCardNumber(card.number);
    if (cardNumber === targetCardNumber) {
      card.number = cardNumber;
      targetCard = card;
    }
  });
  return targetCard;
};

exports.createEncryptedCreditCard = async (unencryptedCreditCard) => {
  if (!this.validateUnencryptedCreditCard(unencryptedCreditCard)) {
    throw new Error('invalid');
  }
  const creditCard = await CreditCard.build({
    ...unencryptedCreditCard,
    number: encryptCardNumber(unencryptedCreditCard.number)
  });
  await creditCard.save();
  return creditCard;
};

exports.validateUnencryptedCreditCard = (creditCard, isPartialCheck = false) => {
  if (isPartialCheck) {
    if (creditCard.number && !isValidCreditCardNumber(creditCard.number)) { return false; }
    if (creditCard.cardholderName && !isValidCreditCardholderName(creditCard.cardholderName)) { return false; }
    if (creditCard.limit && !isValidCreditCardLimit(creditCard.limit)) { return false; }
    return true;
  }

  if (!creditCard.number || !creditCard.cardholderName || !creditCard.limit) { return false; }
  return isValidCreditCardNumber(creditCard.number) &&
    isValidCreditCardholderName(creditCard.cardholderName) &&
    isValidCreditCardLimit(creditCard.limit);
};

exports.validateCreditCardCharge = (creditCardCharge) => {
  if (!creditCardCharge.amount || !creditCardCharge.expM || !creditCardCharge.expY || !creditCardCharge.cvc) { return false; }
  return (Number.isInteger(creditCardCharge.amount) && creditCardCharge.amount >= 0) &&
    (/^-?\d+$/.test(creditCardCharge.expM) && creditCardCharge.expM >= 1 && creditCardCharge.expM <= 12) &&
    (/^-?\d+$/.test(creditCardCharge.expY) && creditCardCharge.expY >= 2023 && creditCardCharge.expY <= 2050) &&
    (/^-?\d+$/.test(creditCardCharge.cvc) && creditCardCharge.cvc.length === 3);
};

function isValidCreditCardholderName(value) {
  return /^[A-Za-z ]+$/.test(value);
}

function isValidCreditCardLimit(value) {
  return Number.isInteger(value) && value >= 0;
}

function isValidCreditCardNumber(value) {
  // defer Luhn check to validator.js (Sequelize.Validator)
  // regex source: https://www.regular-expressions.info/creditcard.html
  return Sequelize.Validator.isCreditCard(value) && (
    /^4[0-9]{12}(?:[0-9]{3})?$/.test(value) ||
    /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/.test(value)
  );
}
