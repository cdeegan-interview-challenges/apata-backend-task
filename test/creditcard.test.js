const { createEncryptedCreditCard, getCreditCardByUnencryptedNumber } = require('../services/creditcard');
const { execSync } = require('child_process');

describe('Credit Card Test', () => {
  beforeAll(() => {
    execSync('npx sequelize-cli db:migrate:undo:all');
    execSync('npx sequelize-cli db:migrate');
  });
  
  afterAll(() => {
    // TODO: cleanup test db
  });

  test('non-existent credit card can not be retrieved', async () => {
    let card = null;
    let err = false;
    try {
      card = await getCreditCardByUnencryptedNumber('4242424242424242');
    } catch (e) {
      err = true;
    }
    expect(err).toBeFalsy();
    expect(card).toBeNull();
  });

  test('credit card can be inserted', async () => {
    let err = false;
    try {
      await createEncryptedCreditCard({
        number: '4242424242424242',
        cardholderName: 'John Doe',
        limit: 200000
      });
    } catch (e) {
      err = true;
    }
    expect(err).toBeFalsy();
  });

  test('credit card can be retrieved', async () => {
    let card = null;
    let err = false;
    try {
      card = await getCreditCardByUnencryptedNumber('4242424242424242');
    } catch (e) {
      err = true;
    }
    expect(err).toBeFalsy();
    expect(card).not.toBeNull();
  });
});