const { encryptCardNumber, decryptCardNumber } = require('../utils/crypto');

describe('Validator Test', () => {
  test('card number can be encrypted and decrypted', () => {
    const encryptedCardNumber = encryptCardNumber('4111111111111111');
    const originalCardNumber = decryptCardNumber(encryptedCardNumber);
    expect('4111111111111111').toEqual(originalCardNumber);
  });
});