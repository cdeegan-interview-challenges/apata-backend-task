const { validateUnencryptedCreditCard } = require('../services/creditcard');

describe('Validator Test', () => {
  test('card number containing letters should be invalid', () => {
    expect(validateUnencryptedCreditCard({ number: 'abc' }, true)).toBe(false);
  });
  test('card number failing Luhn check should be invalid', () => {
    expect(validateUnencryptedCreditCard({ number: '4111111111111112' }, true)).toBe(false);
  });
  test('Visa card number should be valid', () => {
    expect(validateUnencryptedCreditCard({ number: '4111111111111111' }, true)).toBe(true);
  });
  test('Mastercard card number should be valid', () => {
    expect(validateUnencryptedCreditCard({ number: '5555555555554444' }, true)).toBe(true);
  });
  test('Non-Visa/Mastercard number should be invalid', () => {
    expect(validateUnencryptedCreditCard({ number: '378282246310005' }, true)).toBe(false);
  });
  test('credit limit less than 0 should be invalid', () => {
    expect(validateUnencryptedCreditCard({ limit: -2 }, true)).toBe(false);
  });
  test('credit limit with non-integer value should be invalid', () => {
    expect(validateUnencryptedCreditCard({ limit: 2.5 }, true)).toBe(false);
  });
  test('cardholder with valid names should be valid', () => {
    expect(validateUnencryptedCreditCard({ cardholderName: 'Francis McGuire' }, true)).toBe(true);
  });
  test('cardholder with numeric values should be invalid', () => {
    expect(validateUnencryptedCreditCard({ cardholderName: 'Francis1 McGuire' }, true)).toBe(false);
  });
});