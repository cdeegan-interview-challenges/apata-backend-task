const express = require('express');
const Boom = require('boom');
const router = express.Router();
const { chargeCreditCard } = require('../utils/stripe');
const { createEncryptedCreditCard, getAllCreditCards, getCreditCardByUnencryptedNumber, validateCreditCardCharge, validateUnencryptedCreditCard } = require('../services/creditcard');

router.get('/', async function(req, res, next) {
  try {
    const cards = await getAllCreditCards();
    return res.json({ cards });
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

router.post('/', async function(req, res, next) {
  try {
    const creditCard = await createEncryptedCreditCard(req.body);
    return res.json(creditCard);
  } catch (e) {
    if (e.message && e.message === 'invalid') { return next(Boom.badRequest()); }
    return next(Boom.badImplementation());
  }
});

router.get('/:cardNumber', async function(req, res, next) {
  try {
    const creditCard = await getCreditCardByUnencryptedNumber(req.params.cardNumber);
    if (creditCard === null) { return next(Boom.notFound()); }
    return res.json(creditCard);
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

router.put('/:cardNumber', async function(req, res, next) {
  try {
    if (!validateUnencryptedCreditCard(req.body, true)) {
      return next(Boom.badRequest());
    }
    const creditCard = await getCreditCardByUnencryptedNumber(req.params.cardNumber);
    await creditCard.update(req.body);
    return res.json({ success: true });
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

router.delete('/:cardNumber', async function(req, res, next) {
  try {
    const creditCard = await getCreditCardByUnencryptedNumber(req.params.cardNumber);
    await creditCard.destroy();
    return res.json({ success: true });
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

router.post('/:cardNumber/charge', async function(req, res, next) {
  try {
    if (!validateCreditCardCharge(req.body)) { return next(Boom.badRequest()); }
    const creditCard = await getCreditCardByUnencryptedNumber(req.params.cardNumber);
    if (creditCard === null) { return next(Boom.notFound()); }
    if (creditCard.balance + req.body.amount > creditCard.limit) { return res.json({ success: false, message: `Insufficient balance, your balance is ${creditCard.balance} and your limit is ${creditCard.limit}` }); }
    const charge = await chargeCreditCard({ ...req.body, number: creditCard.number }, req.body.amount);
    creditCard.update({ balance: creditCard.balance += req.body.amount });
    return res.json({ charge });
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

router.post('/:cardNumber/credit', async function(req, res, next) {
  try {
    if (!req.body.amount || !Number.isInteger(req.body.amount)) { return next(Boom.badData()); }
    const creditCard = await getCreditCardByUnencryptedNumber(req.params.cardNumber);
    if (creditCard === null) { return next(Boom.notFound()); }
    creditCard.update({ balance: creditCard.balance -= req.body.amount });
    return res.json({ success: true, credited: req.body.amount });
  } catch (e) {
    return next(Boom.badImplementation());
  }
});

module.exports = router;
