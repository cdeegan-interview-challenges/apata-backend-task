
# Backend Software Engineer Technical  Exercise  
## Overview:  
As a Backend Software Engineer at Apata, you will be responsible for building and  
maintaining the backend infrastructure that powers our applications. This take-home  
exercise is designed to evaluate your skills in designing and implementing a more complex  
RESTful API for a secure credit card management system with integration to a mocked  
payment gateway API. You should write your solution in either JavaScript, TypeScript  
(preferred language), Java, Scala, or any other strongly typed language.  
## Task:  
Your task is to create a RESTful API for a secure credit card management system integrated  
with a mocked payment gateway API. This API should allow users to:  
- Create a new credit card account with the cardholder's name and credit limit. Credit  
card numbers should be  stored securely  to prevent exposure to bad actors.  
- Retrieve a list of all credit card accounts.  
- Retrieve a specific credit card account by its original card number.  
- Update the credit limit of an existing credit card account.  
- Delete a credit card account.  
- Implement basic credit card transaction functionality, including:  
- Charging a credit card account for a specified amount.  
- Crediting a credit card account for a specified amount.  
- Integrate with a mocked payment gateway API for processing transactions.  
## Requirements:  
1.  Data Storage:  Store credit card numbers securely to prevent unauthorized access.  
You may use encryption or other secure methods for this purpose. Any relevant card  
and cardholder information should be persisted.  
2.  Endpoints:  Implement the following API endpoints:  
○  POST /credit-cards to create a new credit card account.  
○  GET /credit-cards to retrieve all credit card accounts.  
○  GET /credit-cards/{id} to retrieve a specific credit card account by its original  
card number.  
○  PUT /credit-cards/{id} to update the credit limit of a credit card account.  
○  DELETE /credit-cards/{id} to delete a credit card account.  
○  POST /credit-cards/{originalCardNumber}/charge to charge a credit card  
account.  
○  POST /credit-cards/{originalCardNumber}/credit to credit a credit card  
account.

3.  Validation: Implement input validation to ensure that the cardholder's name is not  
empty, the credit limit is a positive number, and the "id" in the URL path is valid.  
4.  Error Handling: Handle errors gracefully and provide informative error messages in  
the API responses.  
5.  Card Types: Credit cards should be either Mastercard or Visa.  
6.  Payment Gateway Integration: Integrate with a payment gateway API for  
processing credit card transactions. You should use a simulated or mocked API for  
this purpose. This payment gateway should accept a cardNumber, amount. The  
response from this endpoint can result in a success or failure.  
7.  Testing:  Ensure that your code is well-tested, covering the API endpoints,  
transaction functionality, and payment gateway interactions.  
8.  Documentation:  Include clear and concise documentation that describes how to run  
the application, make API requests, and any other relevant information.  
## Submission:  
Please submit your completed exercise in a zip file or a GitHub repository link. Ensure that  
your code is well-organized, readable, and includes comments where necessary.  
Additionally, include instructions on how to set up and run your application.  
## Evaluation:  
Candidates will be evaluated based on the following criteria:  
- Correctness and completeness of the functionality.
- Code quality, organization, and readability.  
-  Error handling and validation.  
-  Testing coverage.  
-  Documentation and instructions.  
## Deadline:  
The exercise should be completed and submitted within one week from the date of receipt.  
## Note:  
You are encouraged to use any relevant libraries or frameworks that you are comfortable  
with.  
Feel free to ask questions if you encounter any ambiguities or need clarifications.